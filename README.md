# combined "sentiment" and "snowball-german" with german words from "ml-sentiment" and stemmed these words with "snowball-german" by Florian Kirnbauer

# install

yarn add bitbucket:Florian771/npm-sentiment

# usage typescript

import \* as Sentiment from 'npm-sentiment';
const sentiment = new Sentiment();

# usage typescript english

let text: string = 'You are awful';

let result: number = sentiment.analyze(text, {
language: 'en',
});
console.dir(result); // Score: -2, Comparative: -0.666

# usage typescript german

let text = 'Ich liebe dich';
let result: number = sentiment.analyze(text, {
language: 'de',
});
console.dir(result); // Score: -2, Comparative: -0.666

# usage javascript

var Sentiment = require('npm-sentiment');
var sentiment = new Sentiment();

# usage english

var result = sentiment.analyze('You are awful', {
language: "en"
});
console.dir(result); // Score: -2, Comparative: -0.666

# usage german

var result = sentiment.analyze('Ich liebe dich', {
language: "de"
});
console.dir(result); // Score: -2, Comparative: -0.666

# test

node test
