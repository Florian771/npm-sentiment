var stem = require('snowball-german')
var labels = require('./labelsNotStemmed.json')
var convertGermanUmlaute = require('../../lib/umlaute')

var newLabels = {}
Object.entries(labels).forEach(el => {
  // console.log(el[0])
  // newLabels[stem(el[0])] = el[1]
  // newLabels[convertGermanUmlaute(el[0])] = el[1]
  newLabels[stem(convertGermanUmlaute(el[0]))] = el[1]
});
console.log(newLabels)