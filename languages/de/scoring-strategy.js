var negators = require('./negators.json');

module.exports = {
    apply: function (tokens, cursor, tokenScore) {
        // console.log("cursor", cursor, "tokens", tokens, "tokenScore", tokenScore)
        if (tokenScore > 0) {
            negative = false
        }

        if (cursor > 0) {
            var nexttokenplus = tokens[cursor + 2];
            if (negators[nexttokenplus]) {
                tokenScore = -tokenScore;
            }
        }
        if (cursor > 0) {
            var nexttoken = tokens[cursor + 1];
            if (negators[nexttoken]) {
                // console.log("negatorstokens[cursor + 1]", negators[nexttoken])
                tokenScore = -tokenScore;
            }
        }
        if (cursor > 0) {
            var prevtoken = tokens[cursor - 1];
            if (negators[prevtoken]) {
                tokenScore = -tokenScore;
            }
        }
        if (cursor > 1) {
            var prevtokenplus = tokens[cursor - 2];
            if (negators[prevtokenplus]) {
                tokenScore = -tokenScore;
            }
        }
        return tokenScore;
    }
};