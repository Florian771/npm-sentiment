var stem = require('snowball-german')
var convertGermanUmlaute = require('./umlaute')

String.prototype.convertGermanUmlaute = function () {
  return convertGermanUmlaute(this)
}

/*eslint no-useless-escape: "off"*/

/**
 * Remove special characters and return an array of tokens (words).
 * @param  {string} input Input string
 * @return {array}        Array of tokens
 */
module.exports = function (input) {
  return input
    .toLowerCase()
    .replace(/\n/g, ' ')
    .convertGermanUmlaute()
    .replace(/[.,\/#!$%\^&\*;:{}=_`\"~()]/g, '')
    .split(' ')
    .map(el => stem(el));
};