module.exports = function convertGermanUmlaute(word) {
  const umlaute = {
    'Ä': 'Ae',
    'Ä': 'Ae',
    'Ü': 'Ue',
    'ä': 'ae',
    'ö': 'oe',
    'ü': 'ue',
    'ß': 'ss'
  }
  let chars = word.split('')
  // console.log("chars", chars)
  chars = chars.map(char => {
    if (umlaute[char]) {
      return umlaute[char]
    }
    return char
  })
  return chars.join('')
}